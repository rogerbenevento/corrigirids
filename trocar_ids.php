<?php
require_once "config.php";
require_once "DB.php";

$tabelas = [
	// 'comanda_itens'		=> ['ultimo_id_online' => '142454', 'evento_id' => false], // OK
	// 'comandas'			=> ['ultimo_id_online' => '4010', 'evento_id' => true], // OK
	// 'cores'				=> ['ultimo_id_online' => '10313', 'evento_id' => false], // OK
	// 'cores_produtos'		=> ['ultimo_id_online' => '168184', 'evento_id' => false], // OK
	// 'pedido_tipos'		=> ['ultimo_id_online' => '152', 'evento_id' => false],
	// 'pedidos'			=> ['ultimo_id_online' => '4347', 'evento_id' => true], // OK
	// 'pedidos_itens'		=> ['ultimo_id_online' => '141435', 'evento_id' => false], // OK
	// 'produtos'			=> ['ultimo_id_online' => '78282', 'evento_id' => true] // OK
];

// foreach ($tabelas as $table => $ultimo_id_online) {
// 	// Cria os campos id_antigo e id_novo
// 	$sql = "ALTER TABLE {$table} ADD id_antigo INT, ADD id_novo INT";
// 	$stmt = DB::prepare($sql);
// 	$stmt->execute();
// }

try {
	DB::beginTransaction();

	foreach ($tabelas as $table => $info) {

		// Lista todos os registros
		$sql = "select * from {$table}";
		if ($info['evento_id']) $sql .= " where evento_id = 191";
		else $sql .= " where date(created) >= '2015-02-01'";

		$stmt = DB::prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		// Pega o último id dentro do offline
		$stmt = DB::prepare("select max(id) as ultimo_id_offline from {$table}");
		$stmt->execute();
		$ultimoID = $stmt->fetch();
		$ultimo_id_offline = ($ultimoID) ? $ultimoID->ultimo_id_offline : 1;

		$id = ($info['ultimo_id_online'] <= $ultimo_id_offline) ? $ultimo_id_offline + 1 : $info['ultimo_id_online'];

		echo "Tabela: {$table}\n";

		// Cria os novos ids e altera os ids atuais, mantendo o id antigo no campo "id_antigo"
		foreach ($rows as $r) {
			$sql = "update {$table} set id_antigo = id, id_novo = '{$id}', id = '{$id}' where id = {$r['id']}";
			$stmt = DB::prepare($sql);
			if (!$stmt->execute()) {
				DB::rollback();
				echo $sql ."\n";
				echo "Houve um erro ao fazer a atualizacao.";
				exit();
			}
			$id++;
		}
		echo "OK\n";
		echo "--------------------\n";
	}
	DB::commit();
	echo "Atualizacao concluida com sucesso!!";
} catch (PDOException $e) {
	echo $e->getMessage() ."\n----\n";
}

/*

select max(id) as 'ultimo', 'comanda_itens' as tabela from comanda_itens
union all
select max(id) as 'ultimo', 'comandas' as tabela from comandas
union all
select max(id) as 'ultimo', 'condicoes' as tabela from condicoes
union all
select max(id) as 'ultimo', 'cores' as tabela from cores
union all
select max(id) as 'ultimo', 'cores_produtos' as tabela from cores_produtos
union all
select max(id) as 'ultimo', 'pedido_tipos' as tabela from pedido_tipos
union all
select max(id) as 'ultimo', 'pedidos' as tabela from pedidos
union all
select max(id) as 'ultimo', 'pedidos_itens' as tabela from pedidos_itens
union all
select max(id) as 'ultimo', 'produtos' as tabela from produtos


142453	comanda_itens
4009	comandas
289		condicoes
10312	cores
168183	cores_produtos
151		pedido_tipos
4346	pedidos
141434	pedidos_itens
78281	produtos


ALTER TABLE comanda_itens ADD id_novo int, ADD id_antigo int;
ALTER TABLE comandas ADD id_novo int, ADD id_antigo int;
ALTER TABLE condicoes ADD id_novo int, ADD id_antigo int;
ALTER TABLE cores ADD id_novo int, ADD id_antigo int;
ALTER TABLE cores_produtos ADD id_novo int, ADD id_antigo int;
ALTER TABLE pedido_tipos ADD id_novo int, ADD id_antigo int;
ALTER TABLE pedidos ADD id_novo int, ADD id_antigo int;
ALTER TABLE pedidos_itens ADD id_novo int, ADD id_antigo int;
ALTER TABLE produtos ADD id_novo int, ADD id_antigo int;

ALTER TABLE comanda_itens DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE comandas DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE condicoes DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE cores DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE cores_produtos DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE pedido_tipos DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE pedidos DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE pedidos_itens DROP COLUMN id_novo, DROP COLUMN id_antigo;
ALTER TABLE produtos DROP COLUMN id_novo, DROP COLUMN id_antigo;

 */