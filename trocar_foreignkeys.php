<?php
require 'config.php';
require 'DB.php';

$tabelas = [
	'comanda_itens'		=> ['comandas' => 'comanda_id', 'produtos' => 'produto_id', 'cores_produtos' => 'cor_produto_id'],
	'cores_produtos'	=> ['produtos' => 'produto_id', 'cores' => 'cor_id'],
	'pedidos'			=> ['comandas' => 'comanda_id'],
	// 'pedidos_itens'		=> ['pedidos' => 'pedido_id', 'produtos' => 'produto_id', 'cores_produtos' => 'cor_produto_id'],
];

try {
	DB::beginTransaction();

	foreach ($tabelas as $table => $chaves) {

		echo "Tabela: {$table}\n";

		// Lista todos os itens de uma tabela para substituir os campos
		$sql = "select * from {$table}";
		if ( in_array($table, array('produtos', 'pedidos', 'comandas')) ) {
			$sql .= " where evento_id = 191";
		} else $sql .= " where date(created) >= '2015-02-01'";

		// echo "{$sql}\n";
		$stmt = DB::prepare($sql);
		$stmt->execute();
		$rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

		foreach ($rows as $r) {

			// Lista as chaves que deverão sofrer alteração
			foreach ($chaves as $cTable => $chave) {

				if (!empty($r[$chave])) {

					// Busca o novo valor da chave pelo seu valor antigo(id_antigo)
					$sql = "select id, id_antigo from {$cTable} where id_antigo = {$r[$chave]}";
					// echo "{$sql}\n";
					$stmt = DB::prepare($sql);
					$stmt->execute();
					$novoID = $stmt->fetch();

					if ($novoID && !is_null($novoID->id_antigo)) {
						$sql = "update {$table} set {$chave} = {$novoID->id} where id = {$r['id']}";
						// echo "{$sql}\n";
						$stmt = DB::prepare($sql);
						if (!$stmt->execute()) {

							echo "{$sql}\n\n";

							echo "Ocorreu um erro ao fazer atualizacao do campo {$chave} na tabela {$table}\n";
							echo "--------------------";
						}
					}

				}

			}

		}
		echo "OK!\n";
		echo "--------------------\n";

	}
	DB::commit();
	echo "Atualizacao concluida com sucesso!!";

} catch (PDOException $e) {
	echo $e->getMessage() ."\n";
	echo "--------------------";
}